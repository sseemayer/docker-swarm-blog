#!/usr/bin/env python
import os

from flask import Flask
from flask_restplus import Api, Resource, fields


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')

api = Api(app, version='1.0', title='Demo API', description='A demo API')
ns = api.namespace('blog')

blog = api.model('Blog', {
    'id': fields.Integer(readOnly=True, description='The blog ID'),
    'message': fields.String(required=True, description='The blog message'),
})

from models import db, Blog


@ns.route('/')
class BlogList(Resource):
    @ns.doc('list blogs')
    @ns.marshal_list_with(blog)
    def get(self):
        return Blog.query.all()


    @ns.doc('create blog')
    @ns.expect(blog)
    @ns.marshal_with(blog, code=201)
    def post(self):
        blg = Blog(message=api.payload['message'])
        db.session.add(blg)
        db.session.commit()

        return blg


