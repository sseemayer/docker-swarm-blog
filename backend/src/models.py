from server import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

class Blog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String, nullable=False)


    @property
    def json(self):
        return {
            'id': self.id,
            'message': self.message,
        }


db.create_all()
